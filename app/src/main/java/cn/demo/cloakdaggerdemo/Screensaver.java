package cn.demo.cloakdaggerdemo;

import android.os.Handler;
import android.os.Message;


public class Screensaver {
    private static Screensaver screensaver;
    private static final int MSG_TIMEOUT = 1000;

    private OnTimeOutListener mOnTimeOutListener;
    private int mScreensaverTiemout = 1000*10;//10s as default

    private Handler mHandler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what == MSG_TIMEOUT){

                //Call the callback to notify the receiver
                if(mOnTimeOutListener != null){
                    mOnTimeOutListener.onTimeOut(Screensaver.this);
                }

                start(); //Restart timing
            }
        }
    };

    public static Screensaver getScreensaver(){
        if(screensaver == null){
            screensaver = new Screensaver();
        }
        return screensaver;
    }


    public Screensaver setScreensaver(int screensaverTiemout) {
        mScreensaverTiemout = screensaverTiemout;
        return screensaver;
    }

    private Screensaver() {
    }


    /**
     * start the timer
     */
    public Screensaver start() {
        Message message = mHandler.obtainMessage(MSG_TIMEOUT); //Package message
        mHandler.sendMessageDelayed(message, mScreensaverTiemout); //Delay in sending messages
        return screensaver;
    }

    /**
     * Stop timing
     */
    public void stop() {
        mHandler.removeMessages(MSG_TIMEOUT); //remove the Message
    }

    /**
     * Reset time
     */
    public void resetTime() {
        stop();
        start();
    }

    public void setScreensaverTiemout(int mScreensaverTiemout) {
        this.mScreensaverTiemout = mScreensaverTiemout;
    }

    public int getScreensaverTiemout() {
        return mScreensaverTiemout;
    }

    public Screensaver setOnTimeOutListener(OnTimeOutListener onTimeOutListener) {
        this.mOnTimeOutListener = onTimeOutListener;
        return screensaver;
    }

    /**
     * Screen saver time to monitor
     */
    public interface OnTimeOutListener {
        void onTimeOut(Screensaver screensaver);
    }

}
