package cn.demo.cloakdaggerdemo;

import android.accessibilityservice.AccessibilityService;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;

import com.jeremyliao.liveeventbus.LiveEventBus;

public class RobService extends AccessibilityService {
    @Override
    public void onAccessibilityEvent(AccessibilityEvent event) {
        int eventType = event.getEventType();
        switch (eventType) {
            case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
                break;
            case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
            case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                String className = event.getClassName().toString();
                Log.e("RobService", className);
                if (className.contains("com.facebook.katana")) {
                    LiveEventBus
                            .get("TESTING", String.class)
                            .post(new String());
                    return;
                }
                break;
        }
    }

    @Override
    public void onInterrupt() {

    }
}